<?php

/**
 * @file
 * Contains the CardinalCommerce Centinel MPI thin client.
 */
 
class CentinelClient {
  private $request = array();
  private $response = array();
  private $response_xml = array();

  public function add($name, $value) {
    $this->request[$name] = $value;
  }

  public function getValue($name) {
    if (isset($this->response[$name])) {
      return $this->response[$name];
    }
    else {
      return NULL;
    }
  }
  
  public function getRequest() {
    return $this->request;
  }
  
  public function getResponse() {
    return $this->response;
  }

  public function getRequestXml($url, $timeout) {
    $writer = new XMLWriter();
    $writer->openMemory();
    $writer->setIndent(FALSE);
    $writer->startDocument('1.0', 'UTF-8');
    $writer->startElement('CardinalMPI');

    foreach ($this->request as $name => $value) {
      $writer->writeElement($name, htmlspecialchars($value));
    }

    $writer->writeElement('Timeout', htmlspecialchars($timeout));
    $writer->writeElement('TransactionUrl', htmlspecialchars($url));
    $writer->writeElement('MerchantSystemDate', htmlspecialchars(format_date(time(), 'custom', 'c')));

    $writer->endElement();
    $writer->endDocument();

    $xml = $writer->outputMemory();
    $writer->flush();
    
    return $xml;
  }
  
  public function getResponseXml() {
    return $this->response_xml;
  }

  public function sendHttp($url, $timeout_connect = COMMERCE_CENTINEL_TIMEOUT_CONNECT, $timeout = COMMERCE_CENTINEL_TIMEOUT_READ) {
    if ((strpos($url, "http://") === 0) || (strpos($url, "https://") === 0)) {
      $xml = $this->getRequestXml($url, $timeout);
      $data = 'cmpi_msg=' . urlencode($xml);
      
      $ch = curl_init($url);
      
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,  2);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);  
      curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
      
      $response = curl_exec($ch);
      $succeeded  = curl_errno($ch) == 0 ? TRUE : FALSE;
      curl_close($ch);
      
      if (!$succeeded) {
        $this->setErrorResponse(8030);
      }
      else {
        $this->response_xml = $response;
        $simple_xml = new SimpleXMLElement($response);

        // Bad response received
        if (strcmp($simple_xml->getName(), 'CardinalMPI') !== 0) {
          $this->setErrorResponse(8010);
        }
        else {
          // Set response values
          foreach ($simple_xml->children() as $child) {
            $this->response[$child->getName()] = (string)$child;
          }
        }
      }
    }
    else {
      // Invalid protocol
      $this->setErrorResponse(8000);
    }

    return $this->getResponse();
  }

  private function setErrorResponse($error) {
    $this->response = array(
      'ErrorNo' => $error,
      'ErrorDesc' => $this->errorDesc($error),
    );
  }

  private function errorDesc($error) {
    $error_desc = '';
    
    switch ($error) {
      case 8000:
        $error_desc = t('Protocol Not Recogonized, must be http:// or https://');
        break;

      case 8010:
        $error_desc = t('Unable to Communicate with MAPS Server');
        break;

      case 8020:
        $error_desc = t('Error Parsing XML Response');
        break;

      case 8030:
        $error_desc = t('Communication Timeout Encountered');
        break;
        
      default:
        $error_desc = t('Unknown error code.');
    }

    return $error_desc;
  }
}
